
<?php
session_start();
    require "Format.php";
    $pseudo_responsable=$_POST['pseudo_responsable'];
    if(isset($_POST['valider']))
    {
        include "connect.php";
        $tabExt = array('jpg','gif','png','jpeg');
        $extension = '';
        $query=$conn-> query("SELECT*FROM personne WHERE pseudo='$pseudo_responsable'");
      
        $row = $query->fetch_assoc();
 
        $responsable_bateau=$row['nom'].' '.$row['prenom'];
        
        $nom_bateau=$conn->real_escape_string($_POST['nom_bateau']);
        $matériaux=$conn->real_escape_string($_POST['matériaux']);
        $poids_v=$conn->real_escape_string($_POST['poids_v']);
        $poids_c=$conn->real_escape_string($_POST['poids_c']);
        $long=$conn->real_escape_string($_POST['long']);
        $larg=$conn->real_escape_string($_POST['larg']);
        
        $anneea=$_POST['annee_a'];
        $moisa=$_POST['mois_a'];
        $joura=$_POST['jour_a'];
         
        $anneed=$_POST['annee_d'];
        $moisd=$_POST['mois_d'];
        $jourd=$_POST['jour_d'];
     
        $image=$_FILES['image']['name'];
       
        if($nom_bateau&&$matériaux&&$poids_v&&$poids_c&&$long&&$larg&&$anneea&&$moisa&&$joura&&$anneed&&$moisd&&$jourd&&$image)
            {

            $extension  = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

             $datea = $anneea.'-'.$moisa.'-'.$joura;
             $dated = $anneed.'-'.$moisd.'-'.$jourd;
            

             $allow=array('pdf');
             $temp=explode(".",$_FILES['pdf']['name']);
             $pdf=$_FILES['pdf']['name'];
             $pdftmpname=$_FILES['pdf']['tmp_name'];
             $folder='pdf/';
             move_uploaded_file($pdftmpname,$folder.$pdf);

             $query=$conn-> query("SELECT*FROM bateau WHERE pseudo='$pseudo_responsable' ");
               
             if($query->num_rows==0)
             {
                if(in_array(strtolower($extension),$tabExt))
                {
                    $imagetmpname=$_FILES['image']['tmp_name'];
                    $folder='image/';
                    move_uploaded_file($imagetmpname,$folder.$image);

                    $insert=$conn->query("INSERT INTO bateau(pseudo,Responsable_bateau,nom_bateau,matériaux,poids_vide,poids_charge,longueur,largeur,date_arrivee,date_depart,img,nom_pdf)
                    VALUES ('$pseudo_responsable','$responsable_bateau','$nom_bateau','$matériaux','$poids_v','$poids_c','$long','$larg','$datea','$dated','$image','$pdf')");
                    if($insert!=TRUE)
                    {
                        echo '<div class="alert alert-danger">Problem detect!</div>';    
                    }
                    else
                    {
                        echo '<div class="alert alert-success">Création réussie!</div>';          
                    }
                }
                else
                {
                    echo '<div class="alert alert-danger"> L\'extension du fichier est incorrecte !</div>'; 
                   
                }
             }
                         
               else
               {
                echo '<div class="alert alert-danger">Vous avez déjà créé un bateau!</div>'; 
               }

            }

			else 
            {
                echo '<div class="alert alert-danger">Formulaire incomplet!</div>'; 
            }
    }
        

?>



<html>

<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="Inscription.css" />
	<title> Inscription</title>
</head>

<body>
<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> MENU</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="creer_bateau.php">Créer un bateau</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="modifier_bateau.php">Modifier bateau</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="supprimer_bateau.php">Supprimer bateau</a>
                <div class="dropdown-divider"></div>
				<a class="dropdown-item" href="supprimer_pdf.php">Supprimer pdf</a>
            </div>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>	
                 <input type="button" class="btn btn-link" name="Info Detaillee" value="Navire"  onclick="window.location='responsable_bateau.php'" />	
				<input type="button" class="btn btn-link" name="Deconnexion" value="Deconnexion"  onclick="window.location='index.php'" />
		</nav>
	</header>

  
	<div id= "session">
    <br><br>
	    <h2>
			EDITER LES INFORMATIONS DU BATEAU
        </h2>
        <form  method="post" action="creer_bateau.php" class="formulaire" enctype="multipart/form-data" >
        <?php

       echo'
          <div class="form-group">
          <label for="formGroupExampleInput">PSEUDO</label>
          <input type="text" name="pseudo_responsable" class="form-control" id="formGroupExampleInput" value="'.$_SESSION['pseudo_responsable'].'" placeholder="'.$_SESSION['pseudo_responsable'].'" readonly>
         </div> 
          ';

        ?>
           
            <br><br>
            <div class="form-group">
                <label for="formGroupExampleInput">NOM DU BATEAU</label>
                <input type="text" name="nom_bateau" class="form-control" id="formGroupExampleInput" placeholder="saisir">
            </div>
           <br><br>
           <div class="form-group">
                <label for="formGroupExampleInput">MATERIAUX DE CONSTRUCTION</label>
                <input type="text" name="matériaux" class="form-control" id="formGroupExampleInput" placeholder="saisir">
            </div>
           <br><br>
           <div class="form-group">
                <label for="formGroupExampleInput">POIDS A VIDE(en tonne)</label>
                <input type="number" name="poids_v" class="form-control" id="formGroupExampleInput" placeholder="saisir">
            </div>
           <br><br>
           <div class="form-group">
                <label for="formGroupExampleInput">POIDS EN CHARGE(en tonne)</label>
                <input type="number" name="poids_c" class="form-control" id="formGroupExampleInput" placeholder="saisir">
            </div>
            <br><br> 
            <div class="form-group">
                <label for="formGroupExampleInput">LONGUEUR(en m)</label>
                <input type="number" name="long" class="form-control" id="formGroupExampleInput" placeholder="saisir">
            </div>
           <br><br>
           <div class="form-group">
                <label for="formGroupExampleInput">LARGEUR(en m)</label>
                <input type="number" name="larg" class="form-control" id="formGroupExampleInput" placeholder="saisir">
            </div>
            <br><br> 
            <div>
            DATE D'ARRIVEE :
           <select name="jour_a">
              <option value="0">Jour</option>
                 <?php
                   for ($i = 1; $i <= 31; $i++) {echo '<option value="' . $i . '">' . $i . '</option>';}
                 ?>
            </select>
            <select name="mois_a" size="1">
              <option value="01">Janvier</option>
              <option value="02">Février</option>
              <option value="03">Mars</option>
              <option value="04">Avril</option>
              <option value="05">Mai</option>
            </select>
            <select name="annee_a">
              <option value="19">2019</option>
            </select> 
            </div>  
            <br><br>
            <div>
            DATE DE DEPART:
           <select name="jour_d">
              <option value="0">Jour</option>
                 <?php
                   for ($i = 1; $i <= 31; $i++) {echo '<option value="' . $i . '">' . $i . '</option>';}
                 ?>
            </select>
            <select name="mois_d" size="1">
              <option value="06">Juin</option>
              <option value="07">Juillet</option>
              <option value="08">Août</option>
              <option value="09">Septembre</option>
              <option value="10">Octobre</option>
              <option value="11">Novembre</option>
              <option value="12">Décembre</option>
            </select>
            <select name="annee_d">
              <option value="19">2019</option>
            </select>
            </div>    
            <br><br>
          
            <div>
                IMAGE DU NAVIRE:    <input type="file" name="image" >
            </div>
            <br><br>
          
          <div>
             AJOUTER UN PDF DESCRIPTIF:    <input type="file"  name="pdf" accept="application/pdf">
          </div>
          
            <br><br><br>
            <input type="submit" name="valider"value="Valider" class="btn btn-dark"/>
            
        </form>

 <br><br><br>

	</div>
	
    <footer>
		<hr>
		<div class="text-center"  class="card text-white bg-dark mb-3">
			Copyright 2018 © Site de renseignement| Tous droits réservés
			<br/>
        	<img src="logo.png" alt="Logo">
		</div>	      
	</footer>			
</body>

</html>