-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 21, 2018 at 01:01 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ARMADA`
--

-- --------------------------------------------------------

--
-- Table structure for table `bdd_5_4_Responsable_bateau`
--

CREATE TABLE `bdd_5_4_Responsable_bateau` (
  `id_responsable` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bdd_5_4_Responsable_bateau`
--

INSERT INTO `bdd_5_4_Responsable_bateau` (`id_responsable`, `pseudo`) VALUES
(4, 'mc'),
(5, 'Ach'),
(6, 'fa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bdd_5_4_Responsable_bateau`
--
ALTER TABLE `bdd_5_4_Responsable_bateau`
  ADD PRIMARY KEY (`id_responsable`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bdd_5_4_Responsable_bateau`
--
ALTER TABLE `bdd_5_4_Responsable_bateau`
  MODIFY `id_responsable` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
