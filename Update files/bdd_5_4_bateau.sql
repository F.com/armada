-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 21, 2018 at 01:05 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ARMADA`
--

-- --------------------------------------------------------

--
-- Table structure for table `bdd_5_4_bateau`
--

CREATE TABLE `bdd_5_4_bateau` (
  `id_bateau` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `Responsable_bateau` varchar(255) NOT NULL,
  `nom_bateau` varchar(255) NOT NULL,
  `matériaux` varchar(255) NOT NULL,
  `poids_vide` float NOT NULL,
  `poids_charge` float NOT NULL,
  `longueur` float NOT NULL,
  `largeur` float NOT NULL,
  `date_arrivee` date NOT NULL,
  `date_depart` date NOT NULL,
  `img` varchar(255) NOT NULL,
  `nom_pdf` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bdd_5_4_bateau`
--

INSERT INTO `bdd_5_4_bateau` (`id_bateau`, `pseudo`, `Responsable_bateau`, `nom_bateau`, `matériaux`, `poids_vide`, `poids_charge`, `longueur`, `largeur`, `date_arrivee`, `date_depart`, `img`, `nom_pdf`) VALUES
(30, 'fa', 'FREE Amen', 'ORLANDO', 'bois, acier, fer', 123, 1234, 1234, 1234, '2019-05-06', '2019-07-15', 'armada.jpg', 'Corrigé-DS1-PIC.pdf'),
(31, 'Ach', 'IMOROU Ach-Raf', 'BLOOM', 'acier, bois,fer', 123, 1234, 1234, 1234, '2019-03-04', '2019-09-04', 'Unknown-2.jpeg', 'Cours_Diagramme_des_Classes_ 2015-2016.pdf'),
(32, 'mc', 'CHACHA Michael', 'LUNA', 'bois, acier, fer', 123, 123, 123, 1234, '2019-03-06', '2019-07-23', 'Unknown-1.jpeg', 'ET-UE3-18_mod.pdf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bdd_5_4_bateau`
--
ALTER TABLE `bdd_5_4_bateau`
  ADD PRIMARY KEY (`id_bateau`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bdd_5_4_bateau`
--
ALTER TABLE `bdd_5_4_bateau`
  MODIFY `id_bateau` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
