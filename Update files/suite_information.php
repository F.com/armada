<?php
session_start();
require "Format.php"; ?>
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="information_detaillee.css" />
	<title> Informations_générales</title>
</head>

<body>
<header>
	
</header>	

	<div id= "body">
	<br><br>

    <?php
    $bateau=$_POST['bateau'];

    echo '<h2>INFORMATIONS GENERALES</h2>';



    $sql = "SELECT *FROM bateau WHERE nom_bateau='$bateau'  ";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    echo '
    <div class="text-center"><img widtd="200px" height="200px" src="data:image/jpeg;base64,'.base64_encode($row['img']).'"></img></div>
    <br><br>

    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">'.$row['nom_bateau'].'</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">Responsable du bateau</th>
                <td>'.$row['Responsable_bateau'].'</td>
            </tr>
            <tr>
                <th scope="row">Matériaux de construction</th>
                <td>'.$row['matériaux'].'</td>
            </tr>
            <tr>
                <th scope="row"> Poids à vide</th>
                <td>'.$row['poids_vide'].' tonnes</td>
            </tr>
            <tr>
                <th scope="row">Pois en charge</th>
                <td>'.$row['poids_charge'].' tonnes</td>
            </tr>
            <tr>
                <th scope="row">Longueur</th>
                <td>'.$row['longueur'].' m</td>
            </tr>
            <tr>
                <th scope="row">Largeur</th>
                <td>'.$row['largeur'].' m</td>
            </tr>
            <tr>
                <th scope="row">Date d_arrivee</th>
                <td>'.$row['date_arrivee'].'</td>
            </tr>
            <tr>
                <th scope="row">Date de départ</th>
                <td>'.$row['date_depart'].'</td>
            </tr>
        </tbody>
    </table>

    <br><br>

    <a href="index.php" class="btn btn-secondary btn-lg active" class="text-center" role="button" aria-pressed="true">Return</a>

    <br><br>
    ';

    $conn->close();		

    ?>	

</div>

<footer>
    <hr>
    <div class="text-center"  class="card text-white bg-dark mb-3">
			Copyright 2018 © Site de renseignement| Tous droits réservés
			<br/>
        	<img src="logo.png" alt="Logo">
		</div>	  	    
</footer>

        
</body>
