
<?php require "Format.php"; ?>

<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="information_detaillee.css" />
	<title> Consulter les accès</title>
</head>

<body>
<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> MENU</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="consulter.php">Consulter les accès</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="Modifier.php">Modifier les accès</a>
				</div>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>		
					<input type="button" class="btn btn-link" name="Deconnexion" value="Deconnexion"  onclick="window.location='deconnexion.php'" />
			</nav>
	</header>	

	<div id= "body">
	<br><br>
	<h2>Liste des personnes inscrites</h2>

		<?php
				$sql = "SELECT nom,prenom,pseudo,profil FROM personne";
				$result = $conn->query($sql);
			

				if($result->num_rows>0)
				{
					echo '<br>

					<table class="table">
						<thead>
							<tr>
								<th>NOM</th>
								<th>PRENOM</th>
								<th>PSEUDO</th>
								<th>PROFIL</th>
							</tr>
						</thead>
						<tbody>';
					while ($row = $result->fetch_assoc())
					{
						if($row['profil']==0)
						{
							$name_profil='Admis';
						}
						else if($row['profil']==1)
						{
							$name_profil='Responsable de bateau';
						}
						else
						{
							$name_profil='Administrateur';
						}
						echo '<tr class="info">
						<td>'.$row['nom'].'</td>
						<td>'.$row['prenom'].'</td>
						<td>'.$row['pseudo'].'</td>
						<td>'.$name_profil.'</td>
					</tr>';
						
					}
					echo '</tbody>
						</table>';
		
				}else {
					echo "0 results";
				}
				$conn->close();		
		 ?>	
			
		<br><br><br><br>
	</div>

	<footer>
		<hr>
		<div class="text-center"  class="card text-white bg-dark mb-3">
			Copyright 2018 © Site de renseignement| Tous droits réservés
			<br/>
        	<img src="logo.png" alt="Logo">
		</div>	      
	</footer>
			
</body>

