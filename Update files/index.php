<?php require "Format.php"; ?>

<head>
		<meta charset="utf-8"/>
		<link rel="stylesheet" href="Page_daccueil.css" />
		<title> PAGE D'ACCUEIL</title>
</head>
<body>
	<header>	
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#renseignement">Se renseigner</a>

		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
   			 <span class="navbar-toggler-icon"></span>
  		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#navires"><span class="sr-only">(current)</span>Navires</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="Inscription.php">Inscription</a>
				</li>
			</ul>
			<form class="form-inline my-2 my-lg-0"  method="post" action="connexion.php">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text" id="basic-addon1">@</span>
					</div>
				<input type="text" class="form-control" name="pseudo" placeholder="Pseudo" aria-label="Username" aria-describedby="basic-addon1">
				</div>
				<input class="form-control mr-sm-2" type="password" name="mdp" placeholder="Mot de Passe" aria-label="Mot de passe">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="connexion" >Connexion</button>
			</form>
		</div>
		</nav>
	
		<img src="image.png" class="img-fluid" alt="Responsive image">		
	</header>	
			
		
	<div id= "session">
			<p class="information" id="renseignement" >  
				<br>
				<mark>L'ARMADA</mark>  est un large rassemblement de grands voiliers organisé à Rouen, dans la Seine-Maritime.
				Il est un des évènements importants du monde de la mer. 
				Il a lieu tous les quatre à six ans sur les quais de la Seine, au sein même de la métropole normande.
				L'idée est venue à la suite d'une course transatlantique entre Rouen et New York qui célébrait le centième anniversaire de la traversée de l'Atlantique par la Statue de la Liberté en 19861. 
				La course fut remporté par le catamaran Roger et Gallet skippé par Éric Loizeau et Patrick Tabarly. 
				Les principaux fondateurs sont Patrick Herr et Jean Lecanuet qui cherchaient une idée pour redynamiser les quais de Rouen.
				Ainsi il fut décidé de faire venir les plus grands voiliers du monde à Rouen pour le bicentenaire de la Révolution. 
				La manifestation fut un tel succès qu'elle devint un rendez-vous régulier.
				Au fil des éditions, la panoplie de bateaux invités s'ouvrit entre autres aux navires de guerre (porte-hélicoptères, sous-marins…) et aux péniches.
				C'est une manifestation gratuite pour les visiteurs.
				La septième édition se déroulera du 6 au 16 juin 2019 sur les quais de Rouen, cette manifestation fêtera ses 30 ans d'existence.
			</p>
			
			<h2 id="navires">
				Liste des navires enregistrés
			</h2>
			<br> <br>
			<?php
					$sql = "SELECT *FROM bateau";
					$result = $conn->query($sql);
				

					if($result->num_rows>0)
					{
						
						echo '<div class="card-group">';
							while ($row = $result->fetch_assoc())
							{
								$j++;
								echo '
								
									<div class="card">
										<img width="400px" height="400px" src="image/'.$row['img'].'"></img>
										<div class="card-body">
											<form method="post" action="suite_information_générale.php">
													<input type="submit" name="bateau" value="'.$row['nom_bateau'].'" />		
											</form>
										</div>
									</div>
								
								';
								if($j%3==0)
								echo'<br>';
							}
						echo'</div>';
					}else {
						echo "0 results";
					}
					$conn->close();		
				$conn->close();		
				?>	
			
			<br>
	</div>

	<footer >
		<hr>
		<div class="text-center"  class="card text-white bg-dark mb-3">
			Copyright 2018 © Site de renseignement| Tous droits réservés
			<br/>
        	<img src="logo.png" alt="Logo">
		</div>	    
	</footer>		
				
</body>

