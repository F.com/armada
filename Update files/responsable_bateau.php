
<?php
session_start();
require "Format.php"; ?>

<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="information_detaillee.css" />
	<title>Responsable</title>
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> MENU</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown">

<?php echo'
				<a class="dropdown-item" href="creer_bateau.php">Créer un bateau</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="modifier_bateau.php">Modifier bateau</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="supprimer_bateau.php">Supprimer bateau</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="supprimer_pdf.php">Supprimer pdf</a>
			</div>
';?>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>		
				<input type="button" class="btn btn-link" name="Deconnexion" value="Deconnexion"  onclick="window.location='deconnexion.php'" />
		</nav>
	</header>	

<?php include "information_detaillee.php"; ?>

	<footer>
		<hr>
		<div class="text-center"  class="card text-white bg-dark mb-3">
			Copyright 2018 © Site de renseignement| Tous droits réservés
			<br/>
        	<img src="logo.png" alt="Logo">
		</div>	      
	</footer>
  
</body>
