-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 21, 2018 at 01:05 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ARMADA`
--

-- --------------------------------------------------------

--
-- Table structure for table `bdd_5_4_personne`
--

CREATE TABLE `bdd_5_4_personne` (
  `id_personne` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `profil` int(11) NOT NULL,
  `sexe` varchar(4) NOT NULL,
  `date_de_naissance` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bdd_5_4_personne`
--

INSERT INTO `bdd_5_4_personne` (`id_personne`, `nom`, `prenom`, `pseudo`, `mdp`, `profil`, `sexe`, `date_de_naissance`) VALUES
(15, 'COMLAN', 'Myrtha', 'cm', '202cb962ac59075b964b07152d234b70', 2, 'F', '1999-06-01'),
(16, 'BARGACH', 'Leila', 'bl', '202cb962ac59075b964b07152d234b70', 2, 'F', '2017-03-03'),
(17, 'CHACHA', 'Michael', 'mc', '202cb962ac59075b964b07152d234b70', 1, 'M', '2015-04-03'),
(20, 'IMOROU', 'Ach-Raf', 'Ach', '202cb962ac59075b964b07152d234b70', 1, 'M', '2014-04-02'),
(22, 'CHARME', 'li', 'cl', '202cb962ac59075b964b07152d234b70', 0, 'M', '2016-03-02'),
(23, 'FREE', 'Amen', 'fa', '202cb962ac59075b964b07152d234b70', 1, 'M', '2014-06-04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bdd_5_4_personne`
--
ALTER TABLE `bdd_5_4_personne`
  ADD PRIMARY KEY (`id_personne`),
  ADD UNIQUE KEY `pseudo` (`pseudo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bdd_5_4_personne`
--
ALTER TABLE `bdd_5_4_personne`
  MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
