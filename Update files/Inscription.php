<html>
<?php require "Format.php"; ?>

<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="Inscription.css" />
	<title> Inscription</title>
</head>

<body>
    <header>	
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="index.php#renseignement">Se renseigner</a>

             <a class="nav-link" href="index.php#navires"><span class="sr-only">(current)</span>Navires</a>
            </nav>
        
        </header>	
  
	<div id= "session">
	    <h2>
			Formulaire d'inscription
        </h2>
        <form  method="post" action="suite_inscription.php" class="formulaire">
            <div class="form-group">
                <label for="formGroupExampleInput">NOM</label>
                <input type="text" name="nom" class="form-control" id="formGroupExampleInput" placeholder="saisir">
            </div>
           <br><br>
           <div class="form-group">
                <label for="formGroupExampleInput">PRENOM</label>
                <input type="text" name="prenom" class="form-control" id="formGroupExampleInput" placeholder="saisir">
            </div>
           <br><br>
           <div class="form-group">
                <label for="formGroupExampleInput">PSEUDO</label>
                <input type="text" name="pseudo" class="form-control" id="formGroupExampleInput" placeholder="saisir">
            </div>
           <br><br>
           <div class="form-group">
                <label for="formGroupExampleInput">MOT DE PASSE</label>
                <input type="password" name="mdp" class="form-control" id="formGroupExampleInput" placeholder="saisir">
            </div>

           <br><br>
           <div class="form-group">
                <label for="formGroupExampleInput">RESSAISISSEZ LE MOT DE PASSE</label>
                <input type="password" name="mdp1" class="form-control" id="formGroupExampleInput" placeholder="saisir">
            </div>

           <br><br>
           <div class="form-group">
                    <label for="exampleFormControlSelect1">SEXE</label>
                    <select name="sexe" class="form-control" id="exampleFormControlSelect1">
                        <option value="F">F</option>
                        <option value="M">M</option>
                    </select>
           </div>
           <br><br>
           DATE DE NAISSANCE: 
           <select name="jour">
              <option value="0">Jour</option>
                 <?php
                   for ($i = 1; $i <= 31; $i++) {echo '<option value="' . $i . '">' . $i . '</option>';}
                 ?>
            </select>
            <select name="mois" size="1">
              <option value="01">Janvier</option>
              <option value="02">Février</option>
              <option value="03">Mars</option>
              <option value="04">Avril</option>
              <option value="05">Mai</option>
              <option value="06">Juin</option>
              <option value="07">Juillet</option>
              <option value="08">Août</option>
              <option value="09">Septembre</option>
              <option value="10">Octobre</option>
              <option value="11">Novembre</option>
              <option value="12">Décembre</option>
            </select>
            <select name="annee">
              <option value="0">Année</option>
                <?php
                  for ($i = 2018; $i >= 1930; $i--){echo '<option value="' . $i . '">' . $i . '</option>'; }
                ?>
            </select>   
            <br><br><br>
            <input type="submit" name="valider"value="Valider" class="btn btn-dark"/>
            
        </form>

 <br><br><br>

	</div>
	
    <footer>
		<hr>
		<div class="text-center"  class="card text-white bg-dark mb-3">
			Copyright 2018 © Site de renseignement| Tous droits réservés
			<br/>
        	<img src="logo.png" alt="Logo">
		</div>	      
	</footer>			
</body>

</html>